STM32F4 Discovery and EPD e-ink display
================================
Demo project for E-INK display EPD and EPD kit controlled by STM32F4 Discovery board. Aim of this project is to control EPD display connected by the kit. 
As the example I have been using transfer for [TI processor](http://www.pervasivedisplays.com/kits/ext_kit).
The project has been made as a project for the Design of External Adapters and Embedded Systems courses on [Faculty of Information Technology](http://www.fit.vutbr.cz) on Brno University of Technology (CZ) and it has been supervised by [Mr. Ruzicka](http://www.fit.vutbr.cz/~ruzicka/) and [Mr. Simek](http://www.fit.vutbr.cz/~simekv/).
If you have any problem contact Vojtech Mrazek at my mail xmraze06 at the server stud.fit.vutbr.cz .

![All devices](http://i.imgur.com/z2f9SvM.jpg)

##Accessories
For successful work you will need this accessories

* STM32F4 Discovery Kit
* EPD display kit
* EPD display (tested with 2.7'')
* connection cable

If you cannot use the display kit, make your own.
Proceed in compliance with the datasheet, but remember for logic levels.
It better to use 5V level, but the discovery board MCU uses only 3V level.
It is possible to make your own logic level converter with BS123 transistor.

![Converter](http://rocketnumbernine.s3.amazonaws.com/uploads/2009/04/level-converter-mosfet.jpg)

##Connection
Connection is simple but you will need to use a special connection cable included in the EPD kit.
Few pins are not connected (marked as NC).
Pins are counted left top, second is on right top, third is under the first one etc.

__Conection__

1. 5V (red - VCC)
2. NC
3. NC
4. NC
5. NC
6. PA1 (green - external temperature)
7. PA5 (yellow - SPI clock)
8. PB5 (orange - BUSY)
9. PD7 (brown - PWM)
10. PB3 (black - !RESET)
11. PD5 (red - PANEL_ON - connected with LD8 diode on the STM kit next to MCU USB port)
12. PD6 (white - DISCHARGE)
13. PD3 (silver - BORDER_CONTROL)
14. PA6 (lila - SPI MISO)
15. PA7 (blue - SPI MOSI)
16. NC
17. NC
18. PD4 (orange - !FLASH_CS)
19. PD1 (brown - !EPD_CS)
20. GND (black - GND)

![Connection](http://i.imgur.com/aZhjBJW.jpg "Connection")

##Portation overview
For development I use Attolic True Studio Lite.
After creation of the new project and you will have to copy source files.
It is necessary to mark directories in Perpasive_Displays_Small_EPD/COG as excluded from the build.
Data in directories are included to C files throw C `include` function.

### GPIO 
Few ports including PWM are controlled by general purpose input / output.
To change this setting look into file *EPD_hardware_gpio.c/h*.
Initialization function is named `EPD_initialize_gpio` and its called every time when the display is starting.
It sets all pins to no-pullup mode.
There are lot of wrappers for setting up or reseting pins.

### SPI
For communication with EPD display is used SPI bus.
SPI is controlled by internal peripheral *SPI1*.
Settings is in the file *EPD_hardware_driver.c/h*.
In `epd_spi_init` function is initializing of the peripheral.
It is possible to attach and detach the unit but it is not implemented yet.
If you need it you should make it throw controlling clock to the pheriperal.

### Temperature
For the temperature measuring is used external sensor.
It is connected to internal DAC.
Temperature is counted with this equation

  100-((voltage-1.199)*1000)/10.77

This feature hasn't been tested - in my house I have only one temperature and I didn't put it in the fridge.

### Timing and interrupts
PWM controlling is not using internal peripheral.
It isn't important to wait accurate time, only waiting with the loop is used.

For longer waiting is using interrupt for `SysTick`.
You will have to call `EPD_SysTick_Handler` function each milisecond


##Video

[![ScreenShot](http://img.youtube.com/vi/BkE6xKqknqI/0.jpg)](http://youtu.be/BkE6xKqknqI)

(click for view video on the YouTube)

##Conclusion
This application has been tested with EPD size 2.7'' and EPD kit.
It looks its working.
With this documentation you can make your own portation of this driver to any ARM processor (or another).
For setting I used StdPheriphDriver from STM, so you can convert it to another STM ARM processor.
Another comments or pull requests are welcome.
While building optimalisation wasn't active. 
There should be problem in waiting loop - it's filled by `__NOP()` function.
