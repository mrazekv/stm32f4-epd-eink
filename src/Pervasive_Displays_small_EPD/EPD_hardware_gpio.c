/**
* \file
*
* \brief The functions of EPD GPIO
*
* Copyright (c) 2012-2014 Pervasive Displays Inc. All rights reserved.
*
*  Authors: Pervasive Displays Inc.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*  1. Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*  2. Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "EPD_hardware_gpio.h"

/**
* \brief Set EPD_CS pin to high
*/
void EPD_cs_high (void) {
	GPIO_SetBits(EPD_DATA_PORT, EPD_EPD_CS_PIN);
}

/**
* \brief Set EPD_CS pin to low
*/
void EPD_cs_low (void) {
	GPIO_ResetBits(EPD_DATA_PORT, EPD_EPD_CS_PIN);
}

/**
* \brief Set Flash_CS pin to high
*/
void EPD_flash_cs_high(void) {
	GPIO_SetBits(EPD_DATA_PORT, EPD_FLASH_CS_PIN);
}

/**
* \brief Set Flash_CS pin to low
*/
void EPD_flash_cs_low (void) {
	GPIO_ResetBits(EPD_DATA_PORT, EPD_FLASH_CS_PIN);
}

/**
* \brief Set /RESET pin to high
*/
void EPD_rst_high (void) {
	GPIO_SetBits(EPD_RESET_PORT, EPD_RESET_PIN);
}

/**
* \brief Set /RESET pin to low
*/
void EPD_rst_low (void) {
	GPIO_ResetBits(EPD_RESET_PORT, EPD_RESET_PIN);
}

/**
* \brief Set DISCHARGE pin to high
*/
void EPD_discharge_high (void) {
	GPIO_SetBits(EPD_DATA_PORT, EPD_DISCHARGE_PIN);
}

/**
* \brief Set DISCHARGE pin to low
*/
void EPD_discharge_low (void) {
	GPIO_ResetBits(EPD_DATA_PORT, EPD_DISCHARGE_PIN);
}

/**
* \brief Set Vcc (PANEL_ON) to high
*/
void EPD_Vcc_turn_on (void) {
	GPIO_SetBits(EPD_DATA_PORT, EPD_PANEL_ON_PIN);
}

/**
* \brief Set Vcc (PANEL_ON) to low
*/
void EPD_Vcc_turn_off (void) {
	GPIO_ResetBits(EPD_DATA_PORT, EPD_PANEL_ON_PIN);
}

/**
* \brief Set BORDER_CONTROL pin to high
*/
void EPD_border_high(void) {
	GPIO_SetBits(EPD_DATA_PORT, EPD_BORDER_PIN);
}

/**
* \brief Set BORDER_CONTROL pin to low
*/
void EPD_border_low (void) {
	//set_gpio_low(EPD_PANELON_PORT,EPD_BORDER_PIN);
	GPIO_ResetBits(EPD_DATA_PORT, EPD_BORDER_PIN);
}

/**
* \brief Get BUSY pin status
*/
bool EPD_IsBusy(void) {
	return (bool)GPIO_ReadInputDataBit(EPD_BUSY_PORT, EPD_BUSY_PIN);
}


void EPD_pwm_high() {
	GPIO_SetBits(EPD_DATA_PORT, EPD_PWM_PIN);
}

void EPD_pwm_low() {
	GPIO_ResetBits(EPD_DATA_PORT, EPD_PWM_PIN);
}

/**
* \brief Configure GPIO
*/
void EPD_initialize_gpio(void) {
	/* PB5 busy - IN
	 * PB3 RESET - OUT
	 * PD5 PANEL_ON - OUT
	 * PD3 BRD CONTROL - OUT
	 * PD6 DISCHARGE - OUT
	 * PD4 FLASH_CS - OUT
	 * PD1 EPD_CS - OUT
	 */

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef settings;
	settings.GPIO_Pin = EPD_PANEL_ON_PIN | EPD_BORDER_PIN | EPD_DISCHARGE_PIN | EPD_FLASH_CS_PIN | EPD_EPD_CS_PIN | EPD_PWM_PIN;
	settings.GPIO_Mode = GPIO_Mode_OUT;
	settings.GPIO_OType = GPIO_OType_PP;
	settings.GPIO_PuPd = GPIO_PuPd_NOPULL;
	settings.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_Init(EPD_DATA_PORT, &settings);

	settings.GPIO_Pin = EPD_RESET_PIN;
	GPIO_Init(EPD_RESET_PORT, &settings);

	settings.GPIO_Pin = EPD_BUSY_PIN;
	settings.GPIO_Mode = GPIO_Mode_IN;
	settings.GPIO_PuPd = GPIO_PuPd_NOPULL;
	settings.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_Init(EPD_BUSY_PORT, &settings);
}


