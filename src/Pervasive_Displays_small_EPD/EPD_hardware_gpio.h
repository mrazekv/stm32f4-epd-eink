/**
* \file
*
* \brief The definition of EPD GPIO pins
*
* Copyright (c) 2012-2014 Pervasive Displays Inc. All rights reserved.
*
*  Authors: Pervasive Displays Inc.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*  1. Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*  2. Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Pervasive_Displays_small_EPD.h"
#include "stm32f4xx.h"

#ifndef DISPLAY_HARDWARE_GPIO_H_INCLUDED
#define DISPLAY_HARDWARE_GPIO_H_INCLUDED

/* PB5 busy - IN
	 * PB3 RESET - OUT
	 * PD5 PANEL_ON - OUT
	 * PD3 BRD CONTROL - OUT
	 * PD6 DISCHARGE - OUT
	 * PD4 FLASH_CS - OUT
	 * PD1 EPD_CS - OUT
	 */

#define EPD_BUSY_PORT 		GPIOB
#define EPD_BUSY_PIN  		GPIO_Pin_5
#define EPD_RESET_PORT 		GPIOB
#define EPD_RESET_PIN 		GPIO_Pin_3
#define EPD_DATA_PORT 		GPIOD
#define EPD_PANEL_ON_PIN 	GPIO_Pin_5
#define EPD_BORDER_PIN 		GPIO_Pin_3
#define EPD_DISCHARGE_PIN 	GPIO_Pin_6
#define EPD_FLASH_CS_PIN 	GPIO_Pin_4
#define EPD_EPD_CS_PIN		GPIO_Pin_1
#define EPD_PWM_PIN			GPIO_Pin_7

/******************************************************************************
* GPIO Defines
*****************************************************************************/
bool EPD_IsBusy(void);
void EPD_cs_high (void);
void EPD_cs_low (void);
void EPD_flash_cs_high(void);
void EPD_flash_cs_low (void);
void EPD_rst_high (void);
void EPD_rst_low (void);
void EPD_discharge_high (void);
void EPD_discharge_low (void);
void EPD_Vcc_turn_off (void);
void EPD_Vcc_turn_on (void);
void EPD_border_high(void);
void EPD_border_low (void);
void EPD_initialize_gpio(void);
void EPD_pwm_high(void);
void EPD_pwm_low(void);

#endif	//DISPLAY_HARDWARE_GPIO_H_INCLUDED


