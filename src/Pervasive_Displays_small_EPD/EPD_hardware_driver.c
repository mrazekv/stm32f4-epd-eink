/**
 * \file
 *
 * \brief The initialization and configuration of COG hardware driver
 *
 * Copyright (c) 2012-2014 Pervasive Displays Inc. All rights reserved.
 *
 *  Authors: Pervasive Displays Inc.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <math.h>
#include "EPD_hardware_driver.h"

static  uint16_t EPD_Counter;

/**
 * \brief Set up EPD Timer for 1 mSec interrupts
 *
 * \note
 * desired value: 1mSec
 * actual value:  1.000mSec
 */
static void initialize_EPD_timer(void) {
	//------------------Timer A1----------------------------------------
	//set up Timer_A CCR1 as master timer using ACLK	
	SysTick_Config(SystemCoreClock / 1000); /* each ms */
	EPD_Counter = 0;
}

/**
 * \brief Start Timer
 */
void start_EPD_timer(void) {
	initialize_EPD_timer();
	EPD_Counter = 0;
}

/**
 * \brief Stop Timer
 */
void stop_EPD_timer(void) {
	//SysTick_Config(0);
}

/**
 * \brief Get current Timer after starting a new one
 */
uint32_t get_current_time_tick(void) {
	return EPD_Counter;
}
/**
 * \brief Set current Timer after starting a new one
 */
void set_current_time_tick(uint32_t count) {
     EPD_Counter=count;
}
/**
 * \brief Interrupt Service Routine for system tick counter
 */
void EPD_SysTick_Handler(void) {
	EPD_Counter++;
}

static void __delay_cycles(unsigned int cycles)
{
	for(unsigned int i = 0; i < cycles; i++)
		  __NOP();
}

/**
 * \brief Delay mini-seconds
 * \param ms The number of mini-seconds
 */
void delay_ms(unsigned int ms) {
	unsigned int limit = EPD_Counter + ms;
	while (EPD_Counter <= limit) {
		__NOP();
		//__delay_cycles(SystemCoreClock / 1000);
	}
}

/**
 * \brief Delay mini-seconds
 * \param ms The number of mini-seconds
 */
void sys_delay_ms(unsigned int ms) {
	delay_ms(ms);
}

static void Wait_10us(void) {
	__delay_cycles(SystemCoreClock / 100000);
}

//******************************************************************
//* PWM  Configuration/Control //PWM output : PD3
//******************************************************************

/**
 * \brief The PWM signal starts toggling
 */
void PWM_start_toggle(void) {

}

/**
 * \brief The PWM signal stops toggling.
 */
void PWM_stop_toggle(void) {

}

/**
 * \brief PWM toggling.
 *
 * \param ms The interval of PWM toggling (mini seconds)
 */
void PWM_run(uint16_t ms) {
	start_EPD_timer();
	do {
		EPD_pwm_high();
		__delay_cycles(30);
		EPD_pwm_low();

		__delay_cycles(30);
	} while (get_current_time_tick() < ms); //wait Delay Time
	stop_EPD_timer();
}

//******************************************************************
//* SPI  Configuration
//******************************************************************

/**
 * \brief Configure SPI
 */
void epd_spi_init(void) {
	/* SCLK = PA5
	 * MISO = PA6
	 * MOSI = PA7
	 */
	/* 1. Enable peripheral clock using the following functions */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	/* 2. Enable SCK, MOSI, MISO and NSS GPIO clocks using RCC_AHB1PeriphClockCmd() function. */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	/* 3. Peripherals alternate function:
	  *                 - Connect the pin to the desired peripherals' Alternate
	  *                   Function (AF) using GPIO_PinAFConfig() function
	  *                 - Configure the desired pin in alternate function by:
	  *                   GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AF
	  *                 - Select the type, pull-up/pull-down and output speed via
	  *                   GPIO_PuPd, GPIO_OType and GPIO_Speed members
	  *                 - Call GPIO_Init() function*/
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* 4. Program the Polarity, Phase, First Data, Baud Rate Prescaler, Slave
	  *             Management, Peripheral Mode and CRC Polynomial values using the SPI_Init()
	  *             function. */
	SPI_InitTypeDef SPI_InitStruct;
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct.SPI_NSS =  SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_CRCPolynomial = 0;       /*!< Specifies the polynomial used for the CRC calculation. */
	SPI_Init(SPI1, &SPI_InitStruct);
	/*
	  *          7. Enable the SPI using the SPI_Cmd() function or enable the I2S using
	  *             I2S_Cmd().
	  *             */
	SPI_Cmd(SPI1, ENABLE);
}

/**
 * \brief Initialize SPI
 */
void epd_spi_attach(void) {
	EPD_flash_cs_high();
	EPD_cs_high();
	//epd_spi_init();
}

/**
 * \brief Disable SPI and change to GPIO
 */
void epd_spi_detach(void) {
}

/**
 * \brief SPI synchronous write
 */
void epd_spi_write(unsigned char Data) {
	SPI_I2S_SendData(SPI1, Data);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
}
/**
 * \brief SPI synchronous read
 */
uint8_t epd_spi_read(unsigned char RDATA) {
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
	uint16_t value = SPI_I2S_ReceiveData(SPI1);
	return (uint8_t) value;
}

/**
 * \brief Send data to SPI with time out feature
 *
 * \param data The data to be sent out
 */
uint8_t epd_spi_write_ex(unsigned char Data) {
	/*uint8_t cnt = 200;
	uint8_t flag = 1;
	SPITXBUF = Data;
	while (!(SPIIFG & SPITXIFG)) {
		if ((cnt--) == 0) {
			flag = 0;
			break;
		}
	}*/
	epd_spi_write(Data);
	return 1; //flag;
}

#if (defined COG_V230_G2)
/**
* \brief SPI command
*
* \param register_index The Register Index as SPI Data to COG
* \param register_data The Register Data for sending command data to COG
* \return the SPI read value
*/
uint8_t SPI_R(uint8_t Register, uint8_t Data) {
	uint8_t result;
	EPD_cs_low ();
	epd_spi_write (0x70); // header of Register Index
	epd_spi_write (Register);

	EPD_cs_high ();
	Wait_10us ();
	EPD_cs_low ();

	epd_spi_write (0x73); // header of Register Data of read command
	result=epd_spi_read (Data);

	EPD_cs_high ();

	return result;
}
#endif

/**
* \brief SPI command if register data is larger than two bytes
*
* \param register_index The Register Index as SPI command to COG
* \param register_data The Register Data for sending command data to COG
* \param length The number of bytes of Register Data which depends on which
* Register Index is selected.
*/
void epd_spi_send (unsigned char register_index, unsigned char *register_data,
               unsigned length) {
	EPD_cs_low ();
	epd_spi_write (0x70); // header of Register Index
	epd_spi_write (register_index);

	EPD_cs_high ();
	Wait_10us ();
	EPD_cs_low ();

	//while(1)
		epd_spi_write (0x72); // header of Register Data of write command
	while(length--) {
		epd_spi_write (*register_data++);
	}
	EPD_cs_high ();
}

/**
* \brief SPI command
*
* \param register_index The Register Index as SPI command to COG
* \param register_data The Register Data for sending command data to COG
*/
void epd_spi_send_byte (uint8_t register_index, uint8_t register_data) {
	EPD_cs_low ();
	epd_spi_write (0x70); // header of Register Index
	epd_spi_write (register_index);

	EPD_cs_high ();
	Wait_10us ();
	EPD_cs_low ();
	epd_spi_write (0x72); // header of Register Data
	epd_spi_write (register_data);
	EPD_cs_high ();
}

//******************************************************************
//* Temperature sensor  Configuration
//******************************************************************
// ADC10 interrupt service routine
#ifdef __Internal_Temperature_Sensor

#error "Not defined yet"
/**
 * \brief Get temperature value from ADC
 *
 * \return the Celsius temperature
 */
int16_t get_temperature(void) {
	const uint8_t DegCOffset=0;
	long temp;
	float IntDegC;

	ADC10CTL1 = INCH_10 + ADC10DIV_3; // Temp Sensor ADC10CLK/4
	ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON+ADC10IE;
	ADC10CTL0 |= ENC + ADC10SC;// Sampling and conversion start
	__bis_SR_register(CPUOFF + GIE);// LPM0, ADC10_ISR will force exit
	// oC = ((A10/1024)*1500mV)-986mV)*1/3.55mV = A10*423/1024 - 278
	temp = ADC10MEM;

	ADC10CTL0 |= ENC + ADC10SC;// Sampling and conversion start
	__bis_SR_register(CPUOFF + GIE);// LPM0, ADC10_ISR will force exit
	temp = ADC10MEM;
	ADC10CTL0 |= ENC + ADC10SC;// Sampling and conversion start
	__bis_SR_register(CPUOFF + GIE);// LPM0, ADC10_ISR will force exit
	temp += ADC10MEM;
	temp=temp/2;

	IntDegC =(long)((long)(temp*423)/1024)-(278+DegCOffset);//(long)((long)(temp - 673) * 423) / 1024;

	__no_operation();// SET BREAKPOINT HERE

	return (int8_t) IntDegC;
}
#elif defined __External_Temperature_Sensor

/**
 * \brief Get temperature value from ADC
 *
 * \return the Celsius temperature
 */
int16_t get_temperature(void) {
	float IntDegC;
	int temp;
	ADC_SoftwareStartConv(ADC1);
	while(ADC_GetSoftwareStartConvStatus(ADC1) != RESET){temp = 0;}
	temp = ADC_GetConversionValue(ADC1) >> 8;
	temp = (temp * 3300) / 0xFF;



	//org
	/*
	 temp = (ADC10MEM*5)/2;
	 voltage = (float)((float)temp*2.5)/1024.0;			//(2.5/1024)*ADC=Mcu voltage,Temperature voltage=Mcu voltage*2
	 IntDegC=100.0- (((voltage*1000)/10.77)-111.3);		//100-((Temperature voltage-1.199)*1000)/10.77=IntDegC
	 __no_operation();					  // SET BREAKPOINT HERE
	 */
	//adj
	IntDegC=100.0- (((temp)/10.77)) + 6;		//100-((Temperature voltage-1.199)*1000)/10.77=IntDegC
	return (int16_t) IntDegC;
}
#endif

/**
 * \brief Initialize the temperature sensor
 */
void initialize_temperature(void) {
#ifdef __External_Temperature_Sensor



	 /* ADC init - pin PA1 */
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	  GPIO_InitTypeDef settings;
	  settings.GPIO_Pin = GPIO_Pin_1;
	  settings.GPIO_Mode = GPIO_Mode_AN;
	  settings.GPIO_PuPd = GPIO_PuPd_NOPULL;
	  GPIO_Init(GPIOA, &settings);

	  ADC_CommonInitTypeDef ADC_CommonInitStructure;
	  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
 	  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
     ADC_CommonInit(&ADC_CommonInitStructure);


	  ADC_InitTypeDef adc_init;
	  adc_init.ADC_ContinuousConvMode = DISABLE;
	  adc_init.ADC_DataAlign = ADC_DataAlign_Right;
	  adc_init.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	  //adc_init.ADC_NbrOfConversion = 2;
	  adc_init.ADC_Resolution = ADC_Resolution_8b;
	  adc_init.ADC_ScanConvMode = DISABLE;
	  ADC_Init(ADC1, &adc_init);

	  ADC_Cmd(ADC1, ENABLE);

	  ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_3Cycles);

#endif
}

/**
 * \brief Initialize the EPD hardware setting
 */
void EPD_display_hardware_init(void) {
	EPD_initialize_gpio();
	EPD_Vcc_turn_off();
	epd_spi_init();
	initialize_temperature();
	EPD_cs_low();
	EPD_pwm_low();
	EPD_rst_low();
	EPD_discharge_low();
	EPD_border_low();
	//initialize_EPD_timer();
}

