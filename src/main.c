/**
*****************************************************************************
**
**  File        : main.c
**
**  Abstract    : main function.
**
**  Functions   : main
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed �as is,� without any warranty
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the Atollic TrueSTUDIO(R) toolchain.
**
**
*****************************************************************************
*/

/* Includes */
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include "Pervasive_Displays_small_EPD.h"
#include "image_data.h"


/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */


void delay()
{
	int i, j, k;
	for(k=0; k < 12; k++)
	for(i=0; i < 255; i++)
		for(j = 0; j < 255; j++);

}

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/
int main(void)
{


  /**
  *  IMPORTANT NOTE!
  *  The symbol VECT_TAB_SRAM needs to be defined when building the project
  *  if code has been located to RAM and interrupts are used. 
  *  Otherwise the interrupt table located in flash will be used.
  *  See also the <system_*.c> file and how the SystemInit() function updates 
  *  SCB->VTOR register.  
  *  E.g.  SCB->VTOR = 0x20000000;  
  */

  /* TODO - Add your application code here */
	SystemInit();
  /* Initialize LEDs */
  STM_EVAL_LEDInit(LED3);
  STM_EVAL_LEDInit(LED4);
  STM_EVAL_LEDInit(LED5);
  STM_EVAL_LEDInit(LED6);

  /* Turn on LEDs */
  STM_EVAL_LEDOff(LED3);
  STM_EVAL_LEDOff(LED4);
  STM_EVAL_LEDOff(LED5);
  STM_EVAL_LEDOff(LED6);

  STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);



  /* Initialize EPD hardware */
  EPD_display_init();

  for(;;) {
  		/* User selects which EPD size to run demonstration by changing the
  		 * USE_EPD_Type in image_data.h
  		 * The Image data arrays for each EPD size are defined at image_data.c */
  #if(USE_EPD_Type==USE_EPD144)
  		EPD_display_from_pointer(EPD_144,(uint8_t *)&image_array_144_2,(uint8_t *)&image_array_144_1);
  #elif(USE_EPD_Type==USE_EPD200)
  		EPD_display_from_pointer(EPD_200,(uint8_t *)&image_array_200_2,(uint8_t *)&image_array_200_1);
  #elif(USE_EPD_Type==USE_EPD270)
  /* Due to if loading two image arrays for 2.7", the code size will exceed the MSP430 flash.
   * If using COG_V230_G2 without working with EPD Kit Tool, the demo will load 1st image continuously.*/
  #ifdef COG_V230_G2
  		EPD_display_from_pointer(EPD_270,(uint8_t *)&image_array_270_1,(uint8_t *)&image_array_270_1);
  #else
  		EPD_display_from_pointer(EPD_270,(uint8_t *)&image_array_270_2,(uint8_t *)&image_array_270_1);
  #endif
  #endif
  		STM_EVAL_LEDOn(LED4);
  		/* The interval of two images alternatively change is 10 seconds */
  		while(STM_EVAL_PBGetState(BUTTON_USER) == 0);
  		while(STM_EVAL_PBGetState(BUTTON_USER) == 1);
  		STM_EVAL_LEDOff(LED4);

  #if(USE_EPD_Type==USE_EPD144)
  		EPD_display_from_pointer(EPD_144,(uint8_t *)&image_array_144_1,(uint8_t *)&image_array_144_2);
  #elif(USE_EPD_Type==USE_EPD200)
  		EPD_display_from_pointer(EPD_200,(uint8_t *)&image_array_200_1,(uint8_t *)&image_array_200_2);
  #elif(USE_EPD_Type==USE_EPD270)
  		/* Due to if loading two image arrays for 2.7", the code size will exceed the MSP430 flash.
  		 * If using COG_V230_G2 without working with EPD Kit Tool, the demo will load 1st image continuously.*/
  #ifdef COG_V230_G2
  		EPD_display_from_pointer(EPD_270,(uint8_t *)&image_array_270_1,(uint8_t *)&image_array_270_1);
  #else
  		EPD_display_from_pointer(EPD_270,(uint8_t *)&image_array_270_1,(uint8_t *)&image_array_270_2);
  #endif
  #endif

  		/* The interval of two images alternatively change is 10 seconds */
  		STM_EVAL_LEDOn(LED4);
  		while(STM_EVAL_PBGetState(BUTTON_USER) == 0);
		while(STM_EVAL_PBGetState(BUTTON_USER) == 1);
  		STM_EVAL_LEDOff(LED4);
  	}


  /* Infinite loop */
  int type = 0;
  while (1)
  {
	  /*printf("ADC: %d\n", GetADCValue());
	  while(STM_EVAL_PBGetState(BUTTON_USER) == 0) {}
	  STM_EVAL_LEDOn(LED4);
	  while(STM_EVAL_PBGetState(BUTTON_USER) == 1) {}
	  STM_EVAL_LEDOff(LED4);*/
	  if(type)
	  {
	  STM_EVAL_LEDOn(LED4);
	  delay();
	  STM_EVAL_LEDOff(LED3);
	  delay();
	  delay();
	  delay();
	  STM_EVAL_LEDOn(LED6);
	  delay();
	  STM_EVAL_LEDOff(LED4);
	  delay();
	  delay();
	  delay();
	  STM_EVAL_LEDOn(LED5);
	  delay();
	  STM_EVAL_LEDOff(LED6);
	  delay();
	  delay();
	  delay();
	  STM_EVAL_LEDOn(LED3);
	  delay();
	  STM_EVAL_LEDOff(LED5);
	  delay();
	  delay();
	  delay();
	  }
	  else
	  {

		  STM_EVAL_LEDToggle(LED3);
		  STM_EVAL_LEDToggle(LED4);
		  STM_EVAL_LEDToggle(LED5);
		  STM_EVAL_LEDToggle(LED6);
		  delay();
	  }


	  if(STM_EVAL_PBGetState(BUTTON_USER) == 1)
	  {
		  type = !type;
	      while(STM_EVAL_PBGetState(BUTTON_USER) == 0) {}
	  }




  }
}


/*
 * Callback used by stm32f4_discovery_audio_codec.c.
 * Refer to stm32f4_discovery_audio_codec.h for more info.
 */
void EVAL_AUDIO_TransferComplete_CallBack(uint32_t pBuffer, uint32_t Size){
  /* TODO, implement your code here */
  return;
}

/*
 * Callback used by stm324xg_eval_audio_codec.c.
 * Refer to stm324xg_eval_audio_codec.h for more info.
 */
uint16_t EVAL_AUDIO_GetSampleCallBack(void){
  /* TODO, implement your code here */
  return -1;
}
